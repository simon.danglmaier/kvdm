#include <QApplication>
#include <truthtablecontroller.h>

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);

	TruthTableController c(NUM_OF_INPUTS, NUM_OF_OUTPUTS, { "a", "b", "c", "d" }, { "y" });

	c.show();

	return QApplication::exec();
}
