// MIP
#pragma once

#include <QDebug>
#include <QObject>
#include <QVector>

// Header file, with many Global Constants and variables

// The initial scope of this programm is limited to 4 Input variables and 1 Output
// These restrictions are maybe temporary, who knows
// Most of the code is written, so that in theory it should work with more variables
// without major problems
constexpr auto NUM_OF_INPUTS  = 4;
constexpr auto NUM_OF_OUTPUTS = 1;

// Number of rows is equal to 2^MAX_NUM_OF_INPUTS
constexpr auto NUM_OF_ROWS = 1U << NUM_OF_INPUTS;

enum class EOutput {
	Invalid, // Used as an invalid value in function return values
	Off,     // Represents a logical 0
	On,      // Represents a logical 1
	DontCare // Represents a logical Don't-care value (either 1 or 0)
};

QString toQString(EOutput val);

EOutput fromQString(QString const& str);
EOutput fromStdString(std::string const& str);
EOutput fromQChar(QChar const& ch);

using PrimeImplicant = QSet<int>;

bool toBool(EOutput e);

bool getTreatDontCareAsOne();
void setTreatDontCareAsOne(bool val);

constexpr int prevPowerOfTwo(int num)
{
	// Source: https://www.includehelp.com/cpp-programs/find-next-and-previous-power-of-two-of-a-given-number.aspx
	// If n is already a power of two, we can simply shift it right
	// by 1 place and get the previous power of 2
	// (n&n-1) will be 0 if n is power of two ( for n > = 2 )
	// (n&&!(n&(n-1))) condition will take care of n < 2;
	if ((num && !(num & (num - 1))) == 1) {
		return num;
	}

	// This while loop will run until we get a number which is a power of 2
	while (num & (num - 1)) {
		// Each time we are performing Bit wise And ( & )operation to clear bits
		num = num & (num - 1);
	}
	return num;
}

constexpr int KVMapIndexToTableIndex(int row, int col, int numOfInputs)
{
	if (row < 0 || col < 0 || row > 3 || col > 3) {
		return -1;
	}

	switch (numOfInputs) {
	case 2: {
		constexpr int TABLE[2][2] = { { 0, 1 }, { 2, 3 } };
		return TABLE[row][col];
	}

	case 3: {
		constexpr int TABLE[2][4] = { { 0, 1, 3, 2 }, { 4, 5, 7, 6 } };
		return TABLE[row][col];
	}
	case 4: {
		constexpr int TABLE[4][4] = { { 0, 1, 3, 2 }, { 4, 5, 7, 6 }, { 12, 13, 15, 14 }, { 8, 9, 11, 10 } };
		return TABLE[row][col];
	}
	}

	return -1;
}
