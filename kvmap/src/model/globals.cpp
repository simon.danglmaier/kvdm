#include "globals.h"

static bool treatDontCareAsTrue{ false };

QString toQString(EOutput val)
{
	switch (val) {
	case EOutput::DontCare:
		return "-";
	case EOutput::On:
		return "1";
	case EOutput::Off:
		return "0";
	case EOutput::Invalid:
		return "?";
	}

	// This path should never be reached
	Q_ASSERT(false);
}

EOutput fromQString(const QString& str)
{
	auto trimmed = str.trimmed();

	if (trimmed.length() != 1) {
		return EOutput::Invalid;
	}

	if (trimmed == "1") {
		return EOutput::On;
	} else if (trimmed == "0") {
		return EOutput::Off;
	} else if (trimmed == "-") {
		return EOutput::DontCare;
	} else {
		qWarning() << "String " << trimmed << "could not be converted to truth table value!";
		return EOutput::Invalid;
	}
}

EOutput fromStdString(const std::string& str)
{
	return fromQString(QString::fromStdString(str));
}

EOutput fromQChar(const QChar& ch)
{
	return fromQString(ch);
}

bool toBool(EOutput e)
{
	switch (e) {
	case EOutput::On:
		return true;
	case EOutput::Off:
		return false;
	case EOutput::Invalid:
		return false;
	case EOutput::DontCare:
		return treatDontCareAsTrue;
	}

	return false;
}

void setTreatDontCareAsOne(bool val)
{
	treatDontCareAsTrue = val;
}

bool getTreatDontCareAsOne()
{
	return treatDontCareAsTrue;
}
