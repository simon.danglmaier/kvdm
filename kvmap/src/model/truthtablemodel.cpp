// System Includes
#include <iterator> // std::back_inserter

// Private Includes
#include "truthtablemodel.h"

TruthTableModel::TruthTableModel(int numOfInputs, int numOfOutputs) : TruthTableModel(numOfInputs, numOfOutputs, {}, {})
{
}

TruthTableModel::TruthTableModel(int numOfInputs,
								 int numOfOutputs,
								 QStringList const& inputLabels,
								 QStringList const& outputLabels) :
	m_numOfInputs{ numOfInputs },
	m_numOfOutputs{ numOfOutputs }

{
	Q_ASSERT_X(numOfInputs > 1, "TruthTableModel", "Number of Inputs must be greater than 1");
	Q_ASSERT_X(numOfInputs <= 4, "TruthTableModel", "Number of Inputs must not be greater than 4");

	Q_ASSERT_X(numOfOutputs == 1, "TruthTableModel", "Number of Outputs must be 1");

	qDebug() << "New TruthTableModel created!";
	qDebug() << "Number of Inputs:" << m_numOfInputs;
	qDebug() << "Number of Outputs:" << m_numOfOutputs;

	m_inputLabels = inputLabels;
	m_outputLabels = outputLabels;
	m_table.resize(1 << numOfInputs);
	m_table.fill(EOutput::Off);
}

TruthTableModel::TruthTableModel(TruthTableModel& other) :
	TruthTableModel(other.getNumOfInputs(), other.getNumOfOutputs(), other.m_inputLabels, other.m_outputLabels)
{
	m_table.resize(0);
	std::copy(other.m_table.cbegin(), other.m_table.cend(), std::back_inserter(m_table));
}

int TruthTableModel::getNumOfInputs() const
{
	return m_numOfInputs;
}

int TruthTableModel::getNumOfOutputs() const
{
	return m_numOfOutputs;
}

int TruthTableModel::getNumOfRows() const
{
	return m_table.length();
}

QStringList TruthTableModel::getInputLabels()
{
	return m_inputLabels;
}

QStringList TruthTableModel::getOutputLabels()
{
	return m_outputLabels;
}

bool TruthTableModel::setValue(int idx, const EOutput& val)
{
	if (idx < 0 || idx >= getNumOfRows()) {
		qWarning() << "Index out of bounds: Index:" << idx << "; Number of rows:" << getNumOfRows();
		return false;
	}

	if (m_table.at(idx) != val) {
		m_table[idx] = val;

		emit truthTableChanged();
	}

	return true;
}

QVector<QPair<QString, QString>> TruthTableModel::toStringVector() const
{
	QVector<QPair<QString, QString>> retVal;

	for (auto i = 0; i < getNumOfRows(); i++) {
		auto bin = QString::number(i, 2);

		auto out = toQString(m_table.at(i));

		retVal.append({ bin, out });
	}

	return retVal;
}

QVector<EOutput> TruthTableModel::getTable() const
{
	return m_table;
}

EOutput TruthTableModel::getValue(int idx) const
{
	if (idx < 0 || idx >= getNumOfRows()) {
		qWarning() << "Index out of bounds! Tried to fetch value from table index" << idx << ", but table only has"
				   << m_table.length() << "entries";
		return EOutput::Invalid;
	}

	return m_table.at(idx);
}


int TruthTableModel::getNumOfOnes(bool includeDontCare) const
{
	auto cntOn = static_cast<int>(std::count(m_table.cbegin(), m_table.cend(), EOutput::On));
	auto cntDC = static_cast<int>(std::count(m_table.cbegin(), m_table.cend(), EOutput::DontCare));

	return includeDontCare ? cntDC + cntOn : cntOn;
}
