#include "kvmapindex.h"

KVMapIndex::KVMapIndex()
{
	row = -1;
	col = -1;
}

KVMapIndex::KVMapIndex(int row, int col)
{
	this->row = row;
	this->col = col;
}

KVMapIndex::KVMapIndex(const KVMapIndex& other)
{
	this->row = other.row;
	this->col = other.col;
}
