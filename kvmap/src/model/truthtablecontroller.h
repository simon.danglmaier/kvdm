#pragma once

#include "formulaview.h"
#include "kvmapview.h"
#include "tableview.h"
#include "truthtablemodel.h"

#include <QDebug>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QSharedPointer>

class TruthTableController : public QWidget {
	Q_OBJECT
public:
	explicit TruthTableController(int                numberOfInputs,
								  int                numberOfOutputs,
								  QStringList const& inputLabels  = {},
								  QStringList const& outputLabels = {},
								  QWidget*           parent       = nullptr);

	QSharedPointer<TableView>   getTableView();
	QSharedPointer<FormulaView> getFormulaView();
	QSharedPointer<KVMapView>   getKVMapView();

signals:

public slots:

	// THIS FUNCTION IS ONLY FOR TESTING
	// THIS WILL SET A RANDOM INDECX IN THE TRUTH TABLE
	// TO SOME RANDOM VALUE
	void onTestButtonClicked();

	void onButtonClicked();

	void onToggleButtonClicked();

private:
	QGroupBox* createTableBox();
	QGroupBox* createKVMapBox();
	QGroupBox* createFormulaBox();

	// Model
	QSharedPointer<TruthTableModel> m_model;

	// Views
	QSharedPointer<FormulaView> m_formulaView;
	QSharedPointer<KVMapView>   m_kvMapView;
	QSharedPointer<TableView>   m_tableView;

	// Buttons
	QPushButton* toggleButton;

	QLineEdit* indexLE;
	QLineEdit* valLE;
};
