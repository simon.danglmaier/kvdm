#pragma once

#include <QObject>

struct KVMapIndex {
	KVMapIndex();

	KVMapIndex(int row, int col);

	KVMapIndex(KVMapIndex const& other);

	int row;
	int col;
};

inline uint qHash(KVMapIndex const& key)
{
	return qHash(QString::number(key.col) + QString::number(key.row));
}

inline bool operator==(KVMapIndex const& lhs, KVMapIndex const& rhs)
{
	return rhs.col == lhs.col && rhs.row == lhs.row;
}

inline bool operator!=(KVMapIndex const& lhs, KVMapIndex const& rhs)
{
	return !(lhs == rhs);
}
