#include "truthtablecontroller.h"

#include "globals.h"

#include <QFrame>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QRandomGenerator>
#include <QResizeEvent>
#include <QVBoxLayout>

TruthTableController::TruthTableController(int                numberOfInputs,
										   int                numberOfOutputs,
										   const QStringList& inputLabels,
										   const QStringList& outputLabels,
										   QWidget*           parent) :
	QWidget(parent),
	m_model{ new TruthTableModel(numberOfInputs, numberOfOutputs, inputLabels, outputLabels) },
	m_formulaView{ new FormulaView(m_model, this) }, m_kvMapView{ new KVMapView(m_model, this) },
	m_tableView{ new TableView(m_model, this) }, toggleButton{}, indexLE{}, valLE{}
{
	auto mainLayout = new QHBoxLayout();

	mainLayout->addWidget(createTableBox());
	mainLayout->addWidget(createKVMapBox());
	// mainLayout->addWidget(createFormulaGroupBox());

	setLayout(mainLayout);
}

QSharedPointer<TableView> TruthTableController::getTableView()
{
	return m_tableView;
}

QSharedPointer<FormulaView> TruthTableController::getFormulaView()
{
	return m_formulaView;
}

QSharedPointer<KVMapView> TruthTableController::getKVMapView()
{
	return m_kvMapView;
}

void TruthTableController::onTestButtonClicked()
{
	auto idxUpperBound = m_model->getNumOfRows();
	auto idxLowerBound = 0;

	int idx = QRandomGenerator::global()->bounded(idxLowerBound, idxUpperBound);

	int val = QRandomGenerator::global()->bounded(0, 3);

	EOutput v;
	QString str;

	if (val == 0) {
		v   = EOutput::DontCare;
		str = "-";
	} else if (val == 1) {
		v   = EOutput::On;
		str = "1";
	} else {
		v   = EOutput::Off;
		str = "0";
	}

	qDebug() << "idx:" << idx << "; val:" << str;

	m_model->setValue(idx, v);
}

void TruthTableController::onButtonClicked()
{
	auto idxText = indexLE->text().trimmed();

	bool ok;
	auto idx = idxText.toInt(&ok, 10);

	if (!ok) {
		qWarning() << "Error converting index" << idxText << "to int!";
		return;
	}

	auto valText = valLE->text().trimmed();

	if (valText.length() == 0 || valText.length() > 1) {
		qWarning() << "Invalid value length";
		return;
	}

	if (!(valText == "-" || valText == "0" || valText == "1")) {
		qWarning() << "Invalid value for KVMap! Value:" << valText;
		return;
	}

	EOutput val = fromQString(valText);

	Q_ASSERT(val != EOutput::Invalid);

	qDebug() << "idx:" << idx << "; val:" << valText;

	m_model->setValue(idx, val);
}

void TruthTableController::onToggleButtonClicked()
{
	QString const str{ "Treat DC as On:" };

	auto next = !getTreatDontCareAsOne();

	if (next) {
		toggleButton->setText(str + "TRUE");
	} else {
		toggleButton->setText(str + "FALSE");
	}

	setTreatDontCareAsOne(next);

	qDebug() << (next ? "Treating dont-cares as TRUE!" : "Treating dont-cares as FALSE");

	m_kvMapView->update();
}

QGroupBox* TruthTableController::createTableBox()
{
	auto layout = new QVBoxLayout();

	layout->addWidget(m_tableView.data());

	auto btn = new QPushButton("Insert value", this);

	connect(btn, &QPushButton::clicked, this, &TruthTableController::onButtonClicked);

	auto randomBtn = new QPushButton("Random value", this);

	connect(randomBtn, &QPushButton::clicked, this, &TruthTableController::onTestButtonClicked);

	auto toggleBtn = new QPushButton("Treat DC as On: FALSE", this);

	toggleBtn->setCheckable(true);

	toggleButton = toggleBtn;

	connect(toggleBtn, &QPushButton::clicked, this, &TruthTableController::onToggleButtonClicked);

	auto indexTF = new QLineEdit(this);

	indexTF->setPlaceholderText("Index [0-15]");
	indexLE = indexTF;

	auto valTF = new QLineEdit(this);

	valTF->setPlaceholderText("value [0, 1, -]");
	valLE = valTF;

	layout->addWidget(randomBtn);
	layout->addWidget(toggleBtn);
	layout->addWidget(btn);
	layout->addWidget(valTF);
	layout->addWidget(indexTF);

	auto gBox = new QGroupBox(tr("Table View"));

	gBox->setLayout(layout);

	return gBox;
}

QGroupBox* TruthTableController::createKVMapBox()
{
	auto layout = new QVBoxLayout();

	layout->addWidget(m_kvMapView.data());

	auto gBox = new QGroupBox(tr("Karnaugh Map"));

	gBox->setLayout(layout);

	return gBox;
}
