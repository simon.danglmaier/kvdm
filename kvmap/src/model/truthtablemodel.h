// MIP
#pragma once

// System Includes
#include <QDebug>
#include <QObject>
#include <QSharedPointer>
#include <QVector>

// Private Includes
#include "globals.h"

class TruthTableModel : public QObject {
	Q_OBJECT
public:
	using QSPtr = QSharedPointer<TruthTableModel>;

	// CTor
	explicit TruthTableModel(int numOfInputs, int numOfOutputs);
	explicit TruthTableModel(int                numOfInputs,
							 int                numOfOutputs,
							 QStringList const& inputLabels,
							 QStringList const& outputLabels);
	// explicit TruthTable(QFile& file);

	explicit TruthTableModel(TruthTableModel& other);

	// DTor
	~TruthTableModel() override = default;

	// Getters

	int getNumOfInputs() const;
	int getNumOfOutputs() const;
	int getNumOfRows() const;

	QStringList getInputLabels();
	QStringList getOutputLabels();

	bool setValue(int idx, EOutput const& val);

	void setTreatDontCaresAsOnes(bool val);
	bool getTreatDontCareAsOnes() const;

	// For displaying and printing to file
	// Temporary
	QVector<QPair<QString, QString>> toStringVector() const;

	QVector<EOutput> getTable() const;
	EOutput          getValue(int idx) const;

	int getNumOfOnes(bool includeDontCare) const;

signals:
	// Signal informing the view, that the Truth Table has changed and
	// That it should fetch a new version to display
	void truthTableChanged();

private:
	QStringList m_inputLabels;
	QStringList m_outputLabels;

	int const m_numOfInputs;
	int const m_numOfOutputs;

	QVector<EOutput> m_table;
};
