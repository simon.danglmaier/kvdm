#pragma once

#include "abstracttruthtableview.h"

#include <QWidget>

class TableView : public AbstractTruthTableView {
public:
	TableView(TruthTableModel::QSPtr const& model, QWidget* parent = nullptr);

	// QWidget interface
protected:
	void paintEvent(QPaintEvent* event) override;

	// QWidget interface
public:
	QSize sizeHint() const override;
	QSize minimumSizeHint() const override;
};
