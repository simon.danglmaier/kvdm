#include "formulaview.h"

FormulaView::FormulaView(TruthTableModel::QSPtr const& model, QWidget* parent) : AbstractTruthTableView(model, parent)
{
}

void FormulaView::paintEvent(QPaintEvent* event)
{
	Q_UNUSED(event);
}

QSize FormulaView::sizeHint() const
{
	return QSize{ 100, 100 };
}

QSize FormulaView::minimumSizeHint() const
{
	return QSize{ 100, 100 };
}
