#pragma once

#include "truthtablemodel.h"

#include <QWidget>

class AbstractTruthTableView : public QWidget {
	Q_OBJECT
public:
	explicit AbstractTruthTableView(TruthTableModel::QSPtr const& model, QWidget* parent = nullptr);

	// QWidget interface
protected:
	void paintEvent(QPaintEvent* event) override = 0;

	TruthTableModel::QSPtr m_model;

	// QWidget interface
public:
	QSize sizeHint() const override        = 0;
	QSize minimumSizeHint() const override = 0;
};
