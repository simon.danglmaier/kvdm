#pragma once

#include "abstracttruthtableview.h"

#include <QWidget>

class FormulaView : public AbstractTruthTableView {
public:
	FormulaView(TruthTableModel::QSPtr const& model, QWidget* parent = nullptr);

	// QWidget interface
protected:
	void paintEvent(QPaintEvent* event) override;

	// QWidget interface
public:
	QSize sizeHint() const override;
	QSize minimumSizeHint() const override;
};
