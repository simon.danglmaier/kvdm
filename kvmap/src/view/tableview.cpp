#include "tableview.h"

#include <QFont>
#include <QtWidgets>

TableView::TableView(TruthTableModel::QSPtr const& model, QWidget* parent) : AbstractTruthTableView(model, parent)
{
	setSizeIncrement(QSize(model->getNumOfInputs() + model->getNumOfOutputs(), model->getNumOfRows() + 1));
	setMaximumSize(QSize(100, 350));
}

void TableView::paintEvent(QPaintEvent* event)
{
	Q_UNUSED(event);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);

	QFont font("Monospace");
	font.setStyleHint(QFont::TypeWriter);

	font.setPixelSize(16);

	painter.setFont(font);

	auto height = this->height();
	auto width  = this->width();

	auto rows = m_model->getNumOfRows() + 1;
	auto cols = m_model->getNumOfInputs() + m_model->getNumOfOutputs();

	auto cellHeight = height / rows;
	auto cellWidth  = width / cols;

	// Insert thicker line between in- and outputs
	auto verticalDividerIdx = m_model->getNumOfInputs();
	// Draw base grid
	painter.setPen(Qt::black);

	// Draw row dividers
	for (auto i = 1; i < rows; i++) {
		painter.setPen(Qt::NoPen);

		// p1 is always the left point
		QPoint p1(0, i * cellHeight);
		QPoint p2(width, i * cellHeight);

		QLine line(p1, p2);

		if (i == 1) {
			// TODO: Make this line bold or double
			painter.setPen(Qt::blue);
		} else if ((i - 1) % 8 == 0) {
			painter.setPen(Qt::black);
		} else if ((i - 1) % 4 == 0) {
			// TODO: Make this line dotted or dashed or just smaller
			painter.setPen(Qt::green);
		}

		painter.drawLine(line);

		painter.setPen(Qt::black);
	}

	painter.setPen(Qt::black);

	// Draw column dividers
	for (auto i = 1; i < cols; i++) {
		// p1 is always the top point
		QPoint p1(i * cellWidth, 0);
		QPoint p2(i * cellWidth, height);

		QLine line(p1, p2);

		if (i == verticalDividerIdx) {
			// TODO: Make this line thicker or double
			painter.setPen(Qt::red);
		}

		painter.drawLine(line);

		painter.setPen(Qt::black);
	}

	auto const inputsLabels = m_model->getInputLabels();
	auto const outputLabels = m_model->getOutputLabels();

	QStringList header;

	for (auto col = 0; col < cols; col++) {
		if (col < m_model->getNumOfInputs()) {
			header.append(inputsLabels[col]);
		} else {
			header.append(outputLabels[col - m_model->getNumOfInputs()]);
		}
	}

	for (auto rowIdx = 0; rowIdx < rows; rowIdx++) {
		for (auto colIdx = 0; colIdx < cols; colIdx++) {
			auto topLeft  = QPoint(colIdx * cellWidth, rowIdx * cellHeight);
			auto botRight = QPoint((colIdx + 1) * cellWidth, (rowIdx + 1) * cellHeight);

			auto boundingRect = QRect(topLeft, botRight);

			auto input = QString::number(rowIdx - 1, 2);

			input = input.rightJustified(m_model->getNumOfInputs(), QChar('0'), true);

			if (rowIdx == 0) {
				painter.drawText(boundingRect, Qt::AlignCenter, header.at(colIdx));
			} else {
				auto val = m_model->getValue(rowIdx - 1);

				QString output = toQString(val);

				// If smaller, draw the inputs
				if (colIdx < verticalDividerIdx) {
					painter.drawText(boundingRect, Qt::AlignCenter, input.at(colIdx));
				} else {
					painter.drawText(boundingRect, Qt::AlignCenter, output);
				}
			}
		}
	}
}

QSize TableView::sizeHint() const
{
	return QSize{ 100, 350 };
}

QSize TableView::minimumSizeHint() const
{
	return QSize{ 100, 350 };
}
