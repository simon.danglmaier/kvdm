#pragma once

#include "abstracttruthtableview.h"
#include "globals.h"
#include "kvmapindex.h"

#include <QSet>
#include <QWidget>

class KVMapView : public AbstractTruthTableView {
public:
	KVMapView(TruthTableModel::QSPtr const& model, QWidget* parent = nullptr);

	QSize sizeHint() const override;
	QSize minimumSizeHint() const override;

	// QWidget interface
protected:
	void paintEvent(QPaintEvent* event) override;

private:
	QVector<QPair<KVMapIndex, QSize>> findPrimeImplicants();
	void                              drawPrimeImplicants(QPainter& painter);
	void                              drawText(QPainter& painter, QRect const& r, QString const& text);

	KVMapIndex       findNextOneOrDontCare(KVMapIndex const& index, bool isFirst = false);
	QSize            trimPrimeImplicantSize(QSize const& size);
	QSet<KVMapIndex> expandPrimeImplicant(KVMapIndex const& index, QSize const& size);

	// Returns size of largest rectangle consisting of only ones or dont cares
	QSize getPrimeImplicantSize(KVMapIndex index);
	int   findPrimeImplicantWidth(KVMapIndex index);
	int   findPrimeImplicantHeight(KVMapIndex index);

	void updateModelView();

	bool isRowAllOnes(int row);
	bool isColumnAllOnes(int col);

	// Returned sizes are sorted from largest Area to smalles area
	// e.g.: 2x2, 2x1, 1x2, 1x1
	QVector<QSize> getPossiblePISizes(QSize const& s);

	bool verifyPI(KVMapIndex const& idx, QSize const& size);

	// Checks wether the PI is out of the visible area (PI Row and column are one of the following: 0, 5)
	bool isPIOutOfBounds(KVMapIndex const& idx, QSize const& s);

	// Returns all indexes of a prime implicant, that are on the outer border
	QSet<KVMapIndex> getOuterBorderIndexes(KVMapIndex const& idx, QSize const& size);

	// Mirrors an Index, ONLY if it is on the outer border!!!!!!
	KVMapIndex mirrorIndex(KVMapIndex const& idx);

private slots:

private:
	const QVector<QColor> m_colors{ QColor(230, 25, 75),  QColor(60, 180, 75),  QColor(255, 225, 25),
									QColor(0, 130, 200),  QColor(245, 130, 48), QColor(145, 30, 180),
									QColor(70, 240, 240), QColor(240, 50, 230), QColor(170, 110, 40) };

	int const cellWidth  = 100;
	int const cellHeight = cellWidth;

	int const m_rowCountInner;
	int const m_rowCountOuter;

	int const m_columnCountInner;
	int const m_columnCountOuter;

	QVector<QVector<EOutput>> m_modelView;
};
