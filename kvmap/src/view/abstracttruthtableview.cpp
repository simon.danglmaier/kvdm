#include "abstracttruthtableview.h"

AbstractTruthTableView::AbstractTruthTableView(TruthTableModel::QSPtr const& model, QWidget* parent) :
	QWidget(parent),
	m_model(model)
{
	Q_ASSERT(model != nullptr);

	connect(m_model.data(), &TruthTableModel::truthTableChanged, this, [this]() { this->update(); });
}
