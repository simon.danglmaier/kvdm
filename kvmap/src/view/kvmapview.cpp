#include "kvmapview.h"

#include "globals.h"

#include <QFont>
#include <QPainter>
#include <QtMath>
#include <algorithm>
#include <string>

KVMapView::KVMapView(TruthTableModel::QSPtr const& model, QWidget* parent) :
	AbstractTruthTableView(model, parent), // If the number of inputs is 2 or 3 we only need 2 inner rows, otherwise 4
	m_rowCountInner{ model->getNumOfInputs() == 2 || model->getNumOfInputs() == 3 ? 2 : 4 },
	// The amount of outer rows is always 2 bigger than the inner
	m_rowCountOuter{ m_rowCountInner + 2 },

	// There are only 2 columns, if the KV Map is a square 2x2 map
	m_columnCountInner{ model->getNumOfInputs() == 2 ? 2 : 4 },
	// Number of outer columns is always 2 bigger than the inner columns
	m_columnCountOuter{ m_columnCountInner + 2 }
{
#ifdef DEBUG
	setMaximumSize(QSize(cellWidth * m_columnCountOuter, cellHeight * m_rowCountOuter));
#else
	setMaximumSize(QSize(cellWidth * m_columnCountInner, cellHeight * m_rowCountInner));
#endif

	qDebug() << "Number of Columns (inner, outer):" << m_columnCountInner << "," << m_columnCountOuter;
	qDebug() << "Number of Rows (inner, outer):" << m_rowCountInner << "," << m_rowCountOuter;

	QPalette pal = palette();

	pal.setColor(QPalette::Background, Qt::white);
	setAutoFillBackground(true);
	setPalette(pal);

	m_modelView.resize(m_rowCountOuter);

	for (auto& t : m_modelView) {
		t.resize(m_columnCountOuter);
		t.fill(EOutput::Off);
	}

	update();
}

void KVMapView::paintEvent(QPaintEvent* event)
{
	Q_UNUSED(event);

	QPainter painter(this);

	updateModelView();

	painter.setRenderHint(QPainter::Antialiasing);

	painter.setPen(Qt::black);

	// Draw complete grid
	for (auto row = 0; row < m_rowCountOuter; row++) {
		for (auto col = 0; col < m_columnCountOuter; col++) {
			QRect r(QPoint(col * cellWidth, row * cellHeight), QSize(cellWidth, cellHeight));
			painter.drawRect(r);
		}
	}

	// Draw text
	// TODO: Add support for drawing the indexes
	for (auto row = 0; row < m_rowCountOuter; row++) {
		for (auto col = 0; col < m_columnCountOuter; col++) {
			QRect r(QPoint(col * cellWidth, row * cellHeight), QSize(cellWidth, cellHeight));
			painter.drawRect(r);

			drawText(painter, r, toQString(m_modelView[row][col]));
		}
	}

	drawPrimeImplicants(painter);

#ifdef DEBUG
	// Draw red rectangle over the inner part for debugging
	QRect outline(QPoint(cellWidth, cellHeight), QSize(cellWidth * m_columnCountInner, cellHeight * m_rowCountInner));
	painter.setPen(QPen(Qt::red, 2));

	painter.drawRect(outline);
#endif

	painter.setPen(Qt::black);
}

QVector<QPair<KVMapIndex, QSize>> KVMapView::findPrimeImplicants()
{
	QVector<QPair<KVMapIndex, QSize>> retVal;
	QSet<KVMapIndex>                  primeImplicants;
	QSet<QSet<KVMapIndex>>            piSet;

	for (auto row = 0; row < m_rowCountOuter; row++) {
		for (auto col = 0; col < m_columnCountOuter; col++) {
			auto const val = m_modelView[row][col];
			auto       idx = KVMapIndex{ row, col };

			if (!toBool(val)) {
				// We dont care about off-values, skip to next value
				continue;
			}

			qDebug() << "Found value" << toQString(val) << "at index [" << idx.row << ";" << idx.col << "]";

			auto sizes = getPossiblePISizes(trimPrimeImplicantSize(getPrimeImplicantSize(idx)));

			QString str;

			for (auto const& e : sizes) {
				str += QString::number(e.width()) + "x" + QString::number(e.height()) + ", ";
			}

			qDebug() << "Possible PI Sizes:" << str;

			for (auto const& s : sizes) {
				auto expPI = expandPrimeImplicant(idx, s);

				Q_ASSERT(!expPI.empty());

				auto contained = primeImplicants.contains(expPI);

				if (verifyPI(idx, s) && !contained && !isPIOutOfBounds(idx, s)) {
					primeImplicants.unite(expPI);
					piSet.insert(expPI);
					retVal.append({ idx, s });
				}
			}
		}
	}

	for (auto const& pi : piSet) {
		for (auto const& index : pi) {
			//			if (std::all_of(piSet.cbegin(), piSet.cend(), [pi](QSet<KVMapIndex> const& set) {
			// set.contains(pi);
			//})) {
			//			}
		}
	}

	return retVal;
}

void KVMapView::drawPrimeImplicants(QPainter& painter)
{
	QHash<KVMapIndex, QColor> colorMap;

	auto const drawOffset = 10;
	auto const radius     = 10;

	auto implicants = findPrimeImplicants();

	painter.save();

	auto piCount = 0;

	for (const auto& e : implicants) {
		piCount++;

		qDebug() << "KVMAPINDEX:" << e.first.row << ";" << e.first.col << ";" << e.second;

		auto index = e.first;
		auto size  = e.second;

		auto topLeft = QPoint(cellWidth * (index.col) + (cellWidth / drawOffset),
							  cellHeight * (index.row) + (cellHeight / drawOffset));

		auto botRight = QPoint(cellWidth * ((index.col) + size.width()) - (cellWidth / drawOffset),
							   cellHeight * ((index.row) + size.height()) - (cellHeight / drawOffset));

		QRect        rect = QRect(topLeft, botRight);
		QPainterPath path;

		path.addRoundedRect(rect, radius, radius);

		auto outerIndexes = getOuterBorderIndexes(index, size);

		qDebug() << "OuterIndexes:";

		for (auto idx : outerIndexes) {
			qDebug() << "Index:[" << idx.row << ";" << idx.col << "]";
		}

		qDebug() << "================";

		// Next color to be used
		QColor color = m_colors[piCount % m_colors.length()];

		if (!outerIndexes.empty()) {
			// If the primeimplicant has size 2x2 and is at position 0,0, then we have a special case
			// In this case the Indexes for all 4 edges are created and added to the colorMap (this can only happen in a
			// 4x4 KVMap, so we check the size too)

			if (index.col == 0 && index.col == 0 && size == QSize{ 2, 2 } && m_columnCountInner == 4 &&
				m_rowCountInner == 4) {
				for (auto row : { 0, m_rowCountOuter - 1 }) {
					for (auto col : { 0, m_columnCountOuter - 1 }) {
						colorMap.insert({ row, col }, m_colors[piCount % m_colors.length()]);
					}
				}
			}

			auto colorIndex = std::find_if(outerIndexes.cbegin(), outerIndexes.cend(), [&](auto const& index) {
				return colorMap.contains(index);
			});

			if (colorIndex != outerIndexes.end()) {
				color = colorMap[*colorIndex];

				// Decrease prime implicant count by 1, as to not skip any colors
				piCount--;
			} else {
				for (auto idx : outerIndexes) {
					auto mirroredIndex = mirrorIndex(idx);

					colorMap.insert(idx, color);
					colorMap.insert(mirroredIndex, color);
				}
			}
		}

		auto fillColor   = QColor(color.red(), color.green(), color.blue(), 63);
		auto borderColor = QColor(color.red(), color.green(), color.blue(), 255);

		painter.fillPath(path, fillColor);

		painter.setPen(borderColor);
		painter.drawPath(path);
	}
	qDebug() << "--------------------------------------------";

	painter.restore();
}

void KVMapView::drawText(QPainter& painter, const QRect& r, const QString& text)
{
	painter.save();
	auto font = QFont("Monospace");
	font.setStyleHint(QFont::TypeWriter);
	font.setPixelSize(24);

	painter.setFont(font);

	painter.drawText(r, Qt::AlignCenter, text);

	painter.restore();
}

QSize KVMapView::trimPrimeImplicantSize(const QSize& size)
{
	auto trimmedWidth  = prevPowerOfTwo(size.width());
	auto trimmedHeight = prevPowerOfTwo(size.height());

	return QSize{ trimmedWidth, trimmedHeight };
}

QSet<KVMapIndex> KVMapView::expandPrimeImplicant(KVMapIndex const& index, const QSize& size)
{
	if (index.row == -1 || index.col == -1 || size.height() == -1 || size.width() == -1) {
		return {};
	}

	QSet<KVMapIndex> retVal;

	for (auto col = index.col; col < m_columnCountOuter && col < index.col + size.width(); col++) {
		for (auto row = index.row; row < m_rowCountOuter && row < index.row + size.height(); row++) {
			retVal.insert(KVMapIndex(row, col));
		}
	}

	return retVal;
}

QSize KVMapView::getPrimeImplicantSize(KVMapIndex index)
{
	auto width = findPrimeImplicantWidth(index);

	auto height = findPrimeImplicantHeight(index);

	return { width, height };
}

int KVMapView::findPrimeImplicantWidth(KVMapIndex index)
{
	auto width = 0;

	while (index.col < m_columnCountOuter && toBool(m_modelView[index.row][index.col])) {
		width++;
		index.col++;
	}

	return width;
}

int KVMapView::findPrimeImplicantHeight(KVMapIndex index)
{
	auto height = 0;

	while (index.row < m_rowCountOuter && toBool(m_modelView[index.row][index.col])) {
		index.row++;
		height++;
	}

	return height;
}

void KVMapView::updateModelView()
{
	// Fill the center part of the kvmap
	for (auto row = 0; row < m_rowCountInner; row++) {
		for (auto col = 0; col < m_columnCountInner; col++) {
			auto tableIndex = KVMapIndexToTableIndex(row, col, m_model->getNumOfInputs());
			auto v          = m_model->getValue(tableIndex);

			// Index is offset by 1 in row and column, because we write into the inner fields
			m_modelView[row + 1][col + 1] = v;
		}
	}

	// The Edges of the outer values are one, if the cell on the opposite edge on the inside is 1
	auto const oCHighIdx = m_columnCountOuter - 1; // Outer column high
	auto const oCLowIdx  = 0;                      // outer column low
	auto const iCHighIdx = m_columnCountOuter - 2; // inner column high
	auto const iCLowIdx  = 1;                      // inner column low

	auto const oRHighIdx = m_rowCountOuter - 1; // outer row high
	auto const oRLowIdx  = 0;                   // outer row low
	auto const iRHighIdx = m_rowCountOuter - 2; // inner row high
	auto const iRLowIdx  = 1;                   // inner row low

	m_modelView[oRLowIdx][oCLowIdx]   = m_modelView[iRHighIdx][iCHighIdx]; // table[0,0] = table[4,4] in a max size KV
	m_modelView[oRLowIdx][oCHighIdx]  = m_modelView[iRHighIdx][iCLowIdx];  // table[0,5] = table[4,1]
	m_modelView[oRHighIdx][oCLowIdx]  = m_modelView[iRLowIdx][iCHighIdx];  // table[5,0] = table[1,4]
	m_modelView[oRHighIdx][oCHighIdx] = m_modelView[iRLowIdx][iCLowIdx];   // table[5,5] = table[1,1]

	// Column 0
	auto writeColIdx = oCLowIdx;
	auto readColIdx  = iCHighIdx;

	for (auto row = iRLowIdx; row < oRHighIdx; row++) {
		if (!isRowAllOnes(row)) {
			m_modelView[row][writeColIdx] = m_modelView[row][readColIdx];
		} else {
			m_modelView[row][writeColIdx] = EOutput::Off;
		}
	}

	// Column 5
	writeColIdx = oCHighIdx;
	readColIdx  = iRLowIdx;

	for (auto row = iRLowIdx; row < oRHighIdx; row++) {
		if (!isRowAllOnes(row)) {
			m_modelView[row][writeColIdx] = m_modelView[row][readColIdx];
		} else {
			m_modelView[row][writeColIdx] = EOutput::Off;
		}
	}

	// Row 0
	auto writeRowIdx = oRLowIdx;
	auto readRowIdx  = iRHighIdx;

	for (auto col = iCLowIdx; col < oCHighIdx; col++) {
		if (!isColumnAllOnes(col)) {
			m_modelView[writeRowIdx][col] = m_modelView[readRowIdx][col];
		} else {
			m_modelView[writeRowIdx][col] = EOutput::Off;
		}
	}

	// Row 5
	writeRowIdx = oRHighIdx;
	readRowIdx  = iRLowIdx;

	for (auto col = iCLowIdx; col < oCHighIdx; col++) {
		if (!isColumnAllOnes(col)) {
			m_modelView[writeRowIdx][col] = m_modelView[readRowIdx][col];
		} else {
			m_modelView[writeRowIdx][col] = EOutput::Off;
		}
	}

	qDebug() << "=== BEGIN MODELVIEW ===";

	QString dbgStr;

	for (auto row = 0; row < m_modelView.length(); row++) {
		for (auto col = 0; col < m_modelView[row].length(); col++) {
			dbgStr += toQString(m_modelView[row][col]) + ";";
		}

		qDebug() << dbgStr;
		qDebug() << "------------------";
		dbgStr.clear();
	}

	qDebug() << "=== END MODELVIEW ===";
}

QVector<QSize> KVMapView::getPossiblePISizes(const QSize& s)
{
	Q_ASSERT(s.width() > 0 && s.height() > 0);

	QVector<QSize> const v = {
		{ 1, 1 }, { 1, 2 }, { 1, 4 }, { 2, 1 }, { 2, 2 }, { 2, 4 }, { 4, 1 }, { 4, 2 }, { 4, 4 }
	};

	QVector<QSize> ret;

	for (auto const& elem : v) {
		if (elem.width() <= s.width() && elem.height() <= s.height()) {
			ret.append(elem);
		}
	}

	// Sort predicate is reversed, since we are sorting in descending order (largest PI-Size comes first)
	std::sort(ret.begin(), ret.end(), [](auto const& lhs, auto const& rhs) {
		auto lArea = lhs.width() * lhs.height();
		auto rArea = rhs.width() * rhs.height();

		return lArea > rArea;
	});

	return ret;
}

bool KVMapView::verifyPI(const KVMapIndex& idx, const QSize& size)
{
	for (auto row = 0; row < size.height(); row++) {
		for (auto col = 0; col < size.width(); col++) {
			if (idx.row + row >= m_modelView.size()) {
				qWarning() << "Index out of bounds!!";
				return false;
			}

			if (idx.col + col >= m_modelView[row].size()) {
				qWarning() << "Index out of bounds!!";
				return false;
			}

			if (!toBool(m_modelView[idx.row + row][idx.col + col])) {
				return false;
			}
		}
	}
	return true;
}

bool KVMapView::isPIOutOfBounds(const KVMapIndex& idx, const QSize& s)
{
	// if its any bigger than 1 by x or x by 1, then it is definitely visible
	if (s.width() > 1 && s.height() > 1) {
		return false;
	}

	if ((idx.row == 0 || idx.row == m_rowCountOuter - 1) && s.height() == 1) {
		return true;
	}

	if ((idx.col == 0 || idx.col == m_columnCountOuter - 1) && s.width() == 1) {
		return true;
	}

	return false;
}

QSet<KVMapIndex> KVMapView::getOuterBorderIndexes(const KVMapIndex& idx, const QSize& size)
{
	QSet<KVMapIndex> retVal{};
	auto             indexes = expandPrimeImplicant(idx, size);

	for (auto const& i : indexes) {
		if (i.col == 0 || i.row == 0 || i.col == m_columnCountOuter - 1 || i.row == m_rowCountOuter - 1) {
			retVal.insert(i);
		}
	}

	return retVal;
}

KVMapIndex KVMapView::mirrorIndex(const KVMapIndex& idx)
{
	if (!isPIOutOfBounds(idx, { 1, 1 })) {
		qWarning() << "PI WAS NOT ON THE OUTSIDE!!!";
		return {};
	}

	auto retVal = idx;

	if (retVal.col == 0) {
		retVal.col = m_columnCountOuter - 1;
	} else if (retVal.col == m_columnCountOuter - 1) {
		retVal.col = 0;
	}

	if (retVal.row == 0) {
		retVal.row = m_rowCountOuter - 1;
	} else if (retVal.row == m_rowCountOuter - 1) {
		retVal.row = 0;
	}

	return retVal;
}

bool KVMapView::isRowAllOnes(int row)
{
	for (auto col = 0; col < m_columnCountInner; col++) {
		if (!toBool(m_modelView[row][col + 1])) {
			return false;
		}
	}

	return true;
}

bool KVMapView::isColumnAllOnes(int col)
{
	for (auto row = 0; row < m_rowCountInner; row++) {
		if (!toBool(m_modelView[row + 1][col])) {
			return false;
		}
	}

	return true;
}

QSize KVMapView::sizeHint() const
{
	return QSize{ 600, 600 };
}

QSize KVMapView::minimumSizeHint() const
{
	return QSize{ 600, 600 };
}
