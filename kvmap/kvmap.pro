QT += core gui widgets

TARGET = kvmap

!greaterThan(QT_MAJOR_VERSION, 4) {
    error(Qt Version 5 is required!)
}

CONFIG += debug
CONFIG += c++17


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += debug

INCLUDEPATH += ./src/view/
INCLUDEPATH += ./src/model/

DEFINES += "DEBUG=1"

SOURCES += \
        ./src/main.cpp \
        ./src/model/globals.cpp \
        ./src/model/kvmapindex.cpp \
        ./src/model/truthtablecontroller.cpp \
        ./src/model/truthtablemodel.cpp \
        ./src/view/abstracttruthtableview.cpp \
        ./src/view/formulaview.cpp \
        ./src/view/kvmapview.cpp \
        ./src/view/tableview.cpp


HEADERS += \
    ./src/model/globals.h \
    ./src/model/kvmapindex.h \
    ./src/model/truthtablecontroller.h \
    ./src/model/truthtablemodel.h \
    ./src/view/abstracttruthtableview.h \
    ./src/view/formulaview.h \
    ./src/view/kvmapview.h \
    ./src/view/tableview.h
