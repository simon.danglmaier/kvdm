QT += core gui widgets

TARGET = qmc

!greaterThan(QT_MAJOR_VERSION, 4) {
    error(Qt Version 5 is required!)
}

CONFIG += debug
CONFIG += c++17


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += debug


INCLUDEPATH += ./src/view/
INCLUDEPATH += ./src/ui/
INCLUDEPATH += ./src/model/

SOURCES += \
        ./src/main.cpp \
        ./src/model/line.cpp \
        ./src/model/qmc.cpp \
        ./src/model/truthtablemodel.cpp \
        ./src/view/kvdm_mainwindow.cpp

HEADERS += \
    ./src/model/globals.h \
    ./src/model/line.h \
    ./src/model/qmc.h \
    ./src/model/truthtablemodel.h \
    ./src/view/kvdm_mainwindow.h


FORMS += \
    ./src/ui/kvdm_mainwindow.ui
