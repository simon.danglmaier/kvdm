/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>
#include <QFont>

#include "analogclock.h"

void drawText(QPainter & painter, QRect const & r, QString const & text) {
    painter.save();
    auto font = QFont("Monospace");
    font.setStyleHint(QFont::TypeWriter);
    font.setPixelSize(24);

    painter.setFont(font);


    painter.drawText(r, Qt::AlignCenter, text);

    painter.restore();
}



//! [0] //! [1]
AnalogClock::AnalogClock(QWidget *parent)
//! [0] //! [2]
    : QWidget(parent)
//! [2] //! [3]
{
//! [3] //! [4]
    QTimer *timer = new QTimer(this);
//! [4] //! [5]
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
//! [5] //! [6]
    timer->start(1000);
//! [6]

    setWindowTitle(tr("Analog Clock"));
    resize(400, 400);

    QPalette pal = palette();

    // set black background
    pal.setColor(QPalette::Background, Qt::white);
    this->setAutoFillBackground(true);
    this->setPalette(pal);
    this->show();
//! [7]
}
//! [1] //! [7]

//! [8] //! [9]
void AnalogClock::paintEvent(QPaintEvent *)
//! [8] //! [10]
{

//! [10]

//! [11]
    QPainter painter(this);
//! [11] //! [12]
    painter.setRenderHint(QPainter::Antialiasing);
//! [12] //! [13]
    //painter.translate(width() / 2, height() / 2);
//! [13] //! [14]

    auto rectXSize = 100;
    auto rectYSize = rectXSize;

    painter.setPen(Qt::black);

    auto it = 0;

    for (auto row = 0; row < 4; row++){
        for (auto col = 0; col < 4; col++){
            it++;
            QRect r(QPoint(col*rectXSize, row*rectYSize), QSize(rectXSize, rectYSize));
            painter.drawRect(r);

            drawText(painter, r, QString::number(it));
        }
    }



    QRect p1 = QRect(QPoint(rectXSize*0 + (rectXSize/10), rectYSize * 0 + (rectYSize/10)), QPoint(rectXSize*2 - (rectXSize/10), rectYSize * 2 - (rectYSize/10)));
    QRect p2 = QRect(QPoint(rectXSize*1 + (rectXSize/10), rectYSize * 1 + (rectYSize/10)), QPoint(rectXSize*4 - (rectXSize/10), rectYSize * 4 - (rectYSize/10)));


    QPainterPath path1, path2;

    path1.addRoundedRect(p1, 10, 10);
    path2.addRoundedRect(p2, 10, 10);

    painter.fillPath(path1, QColor(255, 0, 0, 63));
    painter.fillPath(path2, QColor(0, 255, 0, 63));


    painter.setPen(Qt::red);
    painter.drawPath(path1);

    painter.setPen(Qt::green);
    painter.drawPath(path2);

    //For demo purposes 2 prime implicants


//    painter.setPen(Qt::red);

//    painter.drawRoundedRect(p1, 10, 10);



//    painter.drawRoundedRect(p2, 10, 10);

//    painter.fillRect(p1, QColor(255, 0, 0, 127));

//    painter.fillRect(p2, QColor(0, 255, 0, 127));

}
//! [26]
