#ifndef KVDM_MAINWINDOW_H
#define KVDM_MAINWINDOW_H

#include <QMainWindow>
#include <QFile>

#include "truthtablemodel.h"


namespace Ui {
class kvdm_mainwindow;
}

class kvdm_mainwindow : public QMainWindow
{
	Q_OBJECT

signals:
	void newFileLoaded();
	void tableChanged();

public slots:
	void minimizeTable();
	void updateTableView();
	void updatePrimeImplicants();
	void expandTruthTable();

public:
	explicit kvdm_mainwindow(QWidget *parent = nullptr);
	~kvdm_mainwindow();

	void loadFile(const QString &fileName);

private slots:
	void open();
	bool save();
	bool saveAs();
	bool saveFile(const QString &fileName);
	void setCurrentFile(const QString &fileName);
    void CreateEditDialog();
    void truthTableEditDone();


private:
	void createActions();
	void createStatusBar();
	void createMenuBar();

	Ui::kvdm_mainwindow* mUI;

	QString mCurFile;

    kvdm::TruthTable* mTruthTableModel;

    kvdm::LineVector mMimimizeResult;
};

#endif // KVDM_MAINWINDOW_H
