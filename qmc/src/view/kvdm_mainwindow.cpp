#include "kvdm_mainwindow.h"

#include "qmc.h"
#include "ui_kvdm_mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QMenuBar>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QStatusBar>
#include <QTextStream>
#include <QToolBar>
#include <QWindow>

void kvdm_mainwindow::minimizeTable()
{
	mMimimizeResult = qmc::minimizeTruthTable(*mTruthTableModel);

	//mTruthTableModel->setTable(mMimimizeResult);

    emit tableChanged();
}

void kvdm_mainwindow::updateTableView()
{
    mUI->lbTruthTable->clear();

    QString display;

    //TODO: Implement this properly
    for (auto const & elem : mTruthTableModel->getTableAsQVector()) {
        display.append(elem.getLineAsQString() + "\n");
    }

    mUI->lbTruthTable->setText(display);

}

void kvdm_mainwindow::updatePrimeImplicants()
{

    mUI->lbPrimeImplicants->clear();

    QString display;

    //TODO: Implement this properly
    for (auto const & elem : mMimimizeResult) {
        display.append(elem.getLineAsQString() + "\n");
    }

    mUI->lbPrimeImplicants->setText(display);

}

void kvdm_mainwindow::expandTruthTable()
{
    //TODO
}

kvdm_mainwindow::kvdm_mainwindow(QWidget *parent) :
	QMainWindow(parent),
	mUI(new Ui::kvdm_mainwindow) {

	createActions();
	createStatusBar();

	mUI->setupUi(this);

	mUI->pBMinimize->setEnabled(false);


    //MONOSPACE FONT FTW
	QFont font("Monospace");
	font.setStyleHint(QFont::TypeWriter);

	mUI->lbTruthTable->setFont(font);
	mUI->lbPrimeImplicants->setFont(font);
	mUI->pTETableEdit->setFont(font);

	// TODO: Implement proper constructor
	mTruthTableModel = new kvdm::TruthTable(1, 1);

	mUI->pTETableEdit->setEnabled(false);
	mUI->pBEdit->setEnabled(false);
	mUI->pBSaveEdit->setEnabled(false);
}

kvdm_mainwindow::~kvdm_mainwindow() {
	delete mUI;
}

void kvdm_mainwindow::loadFile(const QString &fileName)
{
	mCurFile = fileName;

	QFile file(fileName);
	if (!file.open(QFile::ReadOnly | QFile::Text)) {
		QMessageBox::warning(
			this,
			"Application",
			QString("Cannot open file %1:\n%2.").arg(QDir::toNativeSeparators(fileName), file.errorString()));
		return;
	}

	mTruthTableModel = new kvdm::TruthTable(file);

	// mUI->textEditTruthTable->setPlainText(mTruthTableModel->truthTableToQString());

	mUI->pBMinimize->setEnabled(true);

	mUI->pBEdit->setEnabled(true);

	emit newFileLoaded();
}

void kvdm_mainwindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this);

    if (!fileName.isEmpty()) {
        loadFile(fileName);
    }
}

bool kvdm_mainwindow::save()
{
    //TODO: Implement this
    return true;
}

bool kvdm_mainwindow::saveAs()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if (dialog.exec() != QDialog::Accepted){
        return false;
    }
    return saveFile(dialog.selectedFiles().first());
}

bool kvdm_mainwindow::saveFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, "Application",
                             QString("Cannot write file %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName),
                                  file.errorString()));
        return false;
    }

    QTextStream out(&file);

    //out << label->text();


    setCurrentFile(fileName);
    statusBar()->showMessage("File saved", 2000);
    return true;
}

void kvdm_mainwindow::setCurrentFile(const QString &fileName)
{
    mCurFile = fileName;
    setWindowModified(false);

    QString shownName = mCurFile;
    if (mCurFile.isEmpty())
        shownName = "untitled.txt";
    setWindowFilePath(shownName);
}

void kvdm_mainwindow::createActions() {
    /*
    connect(this, tableChanged(), this, &kvdm_mainwindow::updateTableView);
    connect(this, tableChanged(), this, &kvdm_mainwindow::updatePrimeImplicants);

    connect(this, tableChanged(), this, this->updateTableView());
    */

    auto toolBar = addToolBar("File");
    auto fileMenu = menuBar()->addMenu("File");

    toolBar->setAllowedAreas(Qt::TopToolBarArea);
    toolBar->setMovable(false);


    const QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/images/open.png"));
    QAction* openAct = new QAction(openIcon, "&Open...", this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip("Open an existing file");
    connect(openAct, &QAction::triggered, this, &kvdm_mainwindow::open);
    fileMenu->addAction(openAct);
    toolBar->addAction(openAct);

    toolBar->addSeparator();

    const QIcon saveIcon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
    QAction* saveAct = new QAction(saveIcon, "&Save", this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip("Save the document to disk");
    connect(saveAct, &QAction::triggered, this, &kvdm_mainwindow::save);
    fileMenu->addAction(saveAct);
    toolBar->addAction(saveAct);


    //menuBar()->addSeparator();
/*
    const QIcon exitIcon = QIcon::fromTheme("application-exit");
    QAction* exitAct = menuBar()->addAction(exitIcon, tr("E&xit"), this, &QWidget::close);
    exitAct->setShortcuts(QKeySequence::Quit);

    exitAct->setStatusTip(tr("Exit the application"));
*/

  //  QMenu* editMenu = menuBar()->addMenu(tr("&Edit"));
  //  QToolBar* editToolBar = addToolBar(tr("Edit"));

/*
    QMenu* helpMenu = menuBar()->addMenu(tr("&Help"));
    QAction* aboutAct = helpMenu->addAction(tr("&About"), this, &MainWindow::about);
    aboutAct->setStatusTip(tr("Show the application's About box"));

    QAction* aboutQtAct = helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
*/
}

void kvdm_mainwindow::createStatusBar() {
    statusBar()->showMessage("Ready.");
}

void kvdm_mainwindow::createMenuBar() {

}

void kvdm_mainwindow::CreateEditDialog(){
	//TODO: Implement new TextEdit

    mUI->pBSaveEdit->setEnabled(true);
    mUI->pBEdit->setEnabled(false);

    mUI->pTETableEdit->setPlainText(mTruthTableModel->getTruthTableAsQString());

    mUI->pTETableEdit->setEnabled(true);


}

void kvdm_mainwindow::truthTableEditDone(){

    kvdm::LineVector vect;
    auto editContents = mUI->pTETableEdit->toPlainText().split("\n");


    //TODO: Reading in of truth tables will be reworked, once a proper editing
    //technique for the diagrams and the tables has been figured out
    for(auto elem : editContents) {
        auto line = elem.split(" ");

        if (line.length() > 1) {
			auto arg1 = line[0];
			std::reverse(arg1.begin(), arg1.end());
			auto arg2 = line[2].at(0);

			vect.append(kvdm::Line(arg1, arg2));
		}
	}

	delete mTruthTableModel;

	mTruthTableModel = new kvdm::TruthTable(vect);

	mUI->pTETableEdit->setEnabled(false);
	mUI->pTETableEdit->clear();
	mUI->pBEdit->setEnabled(true);
	mUI->pBSaveEdit->setEnabled(false);

	emit tableChanged();
}
