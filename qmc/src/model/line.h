/*
 * Author: Simon Danglmaier
 * Date: 2018-11-1
 * Desc.: Class Line: represents a line in a truth table
 */

//MIP
#ifndef KVDM_LINE_H
#define KVDM_LINE_H

//----------------
//System includes
//----------------
#include <QVector>
#include <QObject>

//----------------
//Private includes
//----------------
#include "globals.h"

namespace kvdm {

class Line {
public:
	//Default CTor
	Line();

	//CTor
	explicit Line(Cube const & in, bitType const out);

    //The string must only contains '0', '1', '-'; Output must be separated from
    //inputs by a space
    //Everything before the space is interpreted as input, everything after as Output
    //explicit Line(QString const & str);

	explicit Line(QString in, bitType const out);

	Line(QString in, QChar const out);

	//Copy Ctor
	Line(Line const & cube);

	//DTor
	virtual ~Line() = default;

	//Operators
	bool operator==	(Line const & rhs) const;
	bool operator<	(Line const & rhs) const;

	//Getters

	//Used for Output
	QString getLineAsQString()		const ;

	inline int getNumOfInputs()		const { return mInputs.length(); }
	inline int getNumOfOutputs()	const { return 1U; }

	inline Cube getInputs()	const { return mInputs; }
	inline bitType getOutput()	const { return mOutput; }

	int getNumberOfOnes() const;

	//Returns, wether the Line has been marked
	//Used in the Quine McCluskey Minimisation Method to extract Prime Implicants
	inline bool getIsMarked() const { return mIsMarked; }

	//Marks the cube with the given value, should almost always be true
	void markLine(bool val);

	//Fuses 2 Line into 1, if the hamming distance is 1
	//returns true on success, false otherwise
	bool fuseLines(Line const & line);

	//Calculates the hamming distance of 2 Lines
	//which is the amount of places, the 2 Lines differ from one another
	//Only considers the INPUTS, output is ignored, because for QMC it is not relevant
	static size_t calcHammingDistance(Line const & lhs, Line const & rhs);

	//Debugging function, simply asserts, that all the values in the cube are valid
	static void assertLineConsistency(Line const & line);

	//Expands a line with dont Cares in the input vector into
	//all possible combinations of complete lines
	//Eg.:	-0-1 will be expanded to
	//		0001
	//		0011
	//		1001
	//		1011
	//if the current line does not contain only the current line (1001 returns a vector containing only 1001)
	kvdm::LineVector expandLine() const;

	void setOutput(QChar const & ch);
	void setOutput(bitType const & ch);

	//ATTENTION: idx 0 represents the LSB
	//The input is uint on purpose, Qt does not like size_t
	void setInput(bitType const& ch, const int& idx);

	unsigned int inputsToUInt() const;
private:
	//Used for Quine/McCluskey algorithm
	//Unmarked Cubes at the end of 1 QMC iteration represent prime implicants
	bool mIsMarked;

	Cube mInputs;
	bitType mOutput;
};

} // namespace kvdm

#endif //KVDM_LINE_H
