//System Includes


//Private Includes
#include "qmc.h"

using namespace kvdm;

//Eliminate all Lines, were the output is equal to kvdm::OFF
void eliminateMaxTerms(kvdm::LineVector& lines);

//Used for QMC, compare 2 groups and fuse Lines, which have a hamming distance of 1
kvdm::LineVector compareGroups(kvdm::LineVector &lowerGroup, kvdm::LineVector &higherGroup);

//Mark lines, which have not been used
void markLinesAsUnused(kvdm::GroupVector & vect);

//Sort minterms into the group vector based on how many 1's it contains
kvdm::GroupVector sortMinTerms(kvdm::LineVector const& lines, const int& numOfGroups);

//Extract prime implicants for further optimisation
void extractPrimeImplikants(kvdm::LineVector & pi, kvdm::GroupVector const & gv);

//Expand the table to the full size, which is always a power of 2
//2^NumberOfInputs
void expandTable(kvdm::LineVector & expTable, kvdm::LineVector const & origTable);

//Convert all Dont Care (kvdm::DC) to ones (kvdm::ON)
void convertDontCareToOne(kvdm::LineVector & vec);

//Eliminates all primeimplicants, which only consist of ones, generated from dont Cares
void eliminateDontCarePrimeImplicants(kvdm::LineVector & pi, kvdm::LineVector const & expTable);

//In this function all remaining prime Implicants are evaluated and optimised
void eliminateUnnecessaryPrimeImplicants(kvdm::LineVector & pi, kvdm::LineVector const & expTable);

//Removes a line if it appears in 2 primeimplicants
void removeLinesIfDuplicate(kvdm::Line const & line, kvdm::GroupVector & gv);

//ATTENTION: This function is used only for the sorting of a truth table
// this function treats Dont Care Values as 0, which is intentional,
// but may lead to unexpected results, if used for anything else
size_t convertLineToUInt(kvdm::Line line);

void eliminateMaxTerms(kvdm::LineVector& lines) {
	for (int i = 0; i < lines.length(); i++) {
		if(lines.at(i).getOutput() == kvdm::OFF){
			lines.removeAt(i);
			//We set i back 1 place, because it would skip certain values otherwise
			i--;
		}
	}
}

kvdm::GroupVector sortMinTerms(kvdm::LineVector const& lines, int const& numOfGroups)
{
	Q_ASSERT(numOfGroups > 0);

	kvdm::GroupVector retVal;

	auto idx = 0;

	retVal.resize(numOfGroups);

	for (auto & elem : lines) {
		idx = elem.getNumberOfOnes();

		Q_ASSERT(idx <= kvdm::maxNumOfInputs && idx < retVal.length());

		retVal[idx].append(elem);
	}

	return retVal;
}

// Iterate over every element of lowerGroup and compare it to every element of higherGroup
// If the hamming distance of both are 1, fuse the cubes and store them in a vector
kvdm::LineVector compareGroups(kvdm::LineVector &lowerGroup, kvdm::LineVector &higherGroup) {
	kvdm::LineVector fusedLines;

	for (auto & lhs : lowerGroup) {
		for (auto & rhs : higherGroup) {
			if (Line::calcHammingDistance(lhs, rhs) == 1) {
				Line newLine(lhs);

				if (!newLine.fuseLines(rhs)) {
					qFatal("Error while fusing lines!");
					qFatal(lhs.getLineAsQString().toStdString().data());
					qFatal(rhs.getLineAsQString().toStdString().data());
				}

				lhs.markLine(true);
				rhs.markLine(true);

				fusedLines.append(newLine);
			}
		}
	}

	return fusedLines;
}

void extractPrimeImplicants(kvdm::LineVector& pi, kvdm::GroupVector const& gv)
{
	for (auto const & group : gv) {
		for (auto const & impl : group) {
			qDebug() << "Extracting implicant: isMarked = " << impl.getIsMarked();

			if (!(impl.getIsMarked()) && !pi.contains(impl)) {
				pi.append(impl);
			}
		}
	}
}

void markLinesAsUnused(kvdm::GroupVector & vect) {
	for (auto & group : vect) {
		for (auto & impl : group) {
			impl.markLine(false);
		}
	}
}

void expandTable(kvdm::LineVector & expTable, kvdm::LineVector const & origTable) {
	expTable.clear();

	for (auto const & elem : origTable) {
		auto result = elem.expandLine();

		for (auto const & line : result) {
			expTable.push_back(line);
		}
	}
}

void convertDontCareToOne(kvdm::LineVector & vec) {
	for (auto & elem : vec) {
		if (elem.getOutput() == kvdm::DC) {
			elem.setOutput(kvdm::ON);
		}
	}
}

//Go through all reduced lines, expand them into all possible lines that they could have come from,
//convert them to integers, use that integer to look up the term in the expanded table
void eliminateDontCarePrimeImplicants(kvdm::LineVector & pi, kvdm::LineVector const & expTable) {
	kvdm::LineVector toBeRemoved;

	for (int idx = 0; idx < pi.length(); idx++) {
		auto expanded = pi.at(idx).expandLine();

		//if this is still true after the for loop, then this optimised lines consits only of
		//unmarked 1's and can be deleted
		bool onlyUnmarked = true;

		for (auto const & exp : expanded) {
			onlyUnmarked &= expTable.at(convertToUnsigned(exp.getInputs())).getOutput() == kvdm::DC;
		}

		if (onlyUnmarked) {
			toBeRemoved.push_back(pi[idx]);
		}
	}

	for (auto const & rmElem : toBeRemoved) {
		pi.removeAll(rmElem);
	}
}

size_t convertLineToUInt(kvdm::Line line) {
	for (auto & elem : line.getInputs()) {
		if (elem == kvdm::DC) {
			elem = kvdm::OFF;
		}
	}

	return kvdm::convertToUnsigned(line.getInputs());
}

void eliminateUnnecessaryPrimeImplicants(kvdm::LineVector & pi, kvdm::LineVector const & expTable) {
	kvdm::GroupVector expPI;

	kvdm::LineVector PICopy = pi;

	for (auto const & implicant : pi) {
		expPI.append(implicant.expandLine());
	}

	for (int i = 0; i < expPI.length(); i++) {
		kvdm::LineVector toDelete;

		//find all values, that come from a DontCare Value and delete them
		for (auto const & line : expPI.at(i)) {
			auto index = line.inputsToUInt();
			if(expTable.at(index).getOutput() == kvdm::DC) {
				toDelete.append(line);
			}
		}

		for (auto & toDel : toDelete) {
			expPI[i].removeAll(toDel);
		}
	}

	for (auto & group : expPI) {
		for (auto line : group) {
			removeLinesIfDuplicate(line, expPI);
		}
	}

	//We iterate over the prime implicants backwards, otherwise the indexes would get messed up
	for (int i = expPI.length() - 1; i >= 0; i--) {
		if (expPI.at(i).length() == 0) {
			pi.remove(i);
		}
	}

	qDebug() << "QMC Completed!";
}

void removeLinesIfDuplicate(kvdm::Line const& line, kvdm::GroupVector& gv)
{
	size_t count = 0;

	for (auto const & group : gv) {
		if (group.contains(line)) {
			count++;
		}
	}

	if (count > 1) {
		for (auto & group : gv) {
			group.removeAll(line);
		}
	}
}

LineVector qmc::minimizeTruthTable(const TruthTable& tt)
{
	const auto numberOfGroups = tt.getNumOfInputs() + 1;
	int numOfMinimisations = 0;

	kvdm::LineVector primeImpl;
	kvdm::LineVector expandedTable;
	auto table = tt.getTableAsQVector();

	// Store a copy of the current table, that contains the full
	// 2^(NumOfInputs - 1) rows
	// This makes it easier, to find out, which Cubes contain only unmarked 1
	expandTable(expandedTable, table);

	std::sort(table.begin(), table.end(), [](kvdm::Line const& lhs, kvdm::Line const& rhs) {
		return convertLineToUInt(lhs) < convertLineToUInt(rhs);
	});

	// There must be at least 2 groups
	// Because with only 1 input variable, we have 2 groups
	Q_ASSERT(numberOfGroups >= 2);

	// First: remove all Maxterms (Terms, which output is 0)
	eliminateMaxTerms(table);

	// Convert all dont Care values to 1's, they won't be marked
	// and proceded with the minimisation normally (table consists now only of 1's)
	convertDontCareToOne(table);

	kvdm::GroupVector groupVector = sortMinTerms(table, numberOfGroups);
	markLinesAsUnused(groupVector);

	// Main Loop of Quine McCluskey, run until no terms can be grouped together
	do {
		table.clear();
		for (int i = 0; i < (numberOfGroups - 1); i++) {
			table.append(compareGroups(groupVector[i], groupVector[i + 1]));
		}

		numOfMinimisations = table.length();

		extractPrimeImplicants(primeImpl, groupVector);

		groupVector = sortMinTerms(table, numberOfGroups);

		markLinesAsUnused(groupVector);
	} while (numOfMinimisations > 0);

	eliminateDontCarePrimeImplicants(primeImpl, expandedTable);

	eliminateUnnecessaryPrimeImplicants(primeImpl, expandedTable);

	return primeImpl;
}
