//MIP
#ifndef KVDM_TRUTHTABLE_MODEL_H
#define KVDM_TRUTHTABLE_MODEL_H

//System Includes
#include <QFile>

//Private Includes
#include "globals.h"
#include "line.h"

namespace kvdm{
class TruthTable : public QObject {

signals:
	//Signal informing the view, that the Truth Table has changed and
	//That it should fetch a new version to display
	void truthTableChanged();

public:

	//CTor
    explicit TruthTable(int const numOfInputs, int const numOfOutputs);
    explicit TruthTable(int numOfInputs, int numOfOutputs, QStringList const &inputLabels, QStringList const &outputLabels);
	explicit TruthTable(QFile& file);
    explicit TruthTable(kvdm::LineVector const & vec);

	//DTor
	virtual ~TruthTable() = default;

	//Getters
	kvdm::Line getLine(int idx);

	//kvdm::CubeList getTableAsCubeList()		const;
	kvdm::LineVector getTableAsQVector()	const;
	kvdm::LineVector getExpandedTableAsQVector();

    inline int getNumOfInputs() 	const { return mNumOfInputs; }
    inline int getNumOfOutputs() 	const { return mNumOfOutputs; }
    inline int getNumOfRows() 		const { return mTable.length(); }

	//For displaying and printing to file
	//Temporary
	QString getTruthTableAsQString() const;

	//Setters
	//Set new truth Table, but keep the labels
	void setTable(kvdm::LineVector const & vec);

    inline void setInputLabels (QStringList const &inputLabels) {mInputLabels = inputLabels;}
    inline void setOutputLabels(QStringList const& outputLabels) {mOutputLabels = outputLabels;}

	//Add a cube at the end
	void addLine(kvdm::Line const & line);

	bool containsLine(kvdm::Line const & line);

	//For Debugging and testing
	//Puts random values in the output column
	//void fillTableWithRandomValues();

	//TODO: Implement/Adapt writeToFile
	//void writeToFile(QFile& file);
	void readFromFile(QFile & file);

	//Debugging function
	//Asserts that all the lengths of the cubes match etc
	void assertTruthTableConsistency();

private:
	//States for the FSM Handling reading of files
	enum FSMState{
		eStart,
		eError,
		eInFound,
		eInOutFound,
		eInLblFound,
		eAllFound,
		eProcessTruthTable,
		eEnd
	};

	//Main function for the state machine
	//Gets 1 line of the file as input
	void processLine(QString const & line, FSMState & state);

	QStringList mInputLabels;
	QStringList mOutputLabels;

	int mNumOfInputs;
	int mNumOfOutputs;
	int mNumOfRows;

	kvdm::LineVector mTable;
};

}

#endif //KVDM_TRUTHTABLE_MODEL_H
