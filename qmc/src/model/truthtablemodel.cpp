//System Includes
#include <QTextStream>
#include <QDebug>
#include <algorithm>

//Private Includes
#include "truthtablemodel.h"

//resolving namespace
using namespace kvdm;

//Helper function, that converts the given number to a Binary vector
//void convertToBinary(kvdm::Cube &array, size_t num);

TruthTable::TruthTable(const int numOfInputs, const int numOfOutputs)
{
    mNumOfInputs = numOfInputs;
    mNumOfOutputs = numOfOutputs;
}

TruthTable::TruthTable(int numOfInputs, int numOfOutputs, const QStringList &inputLabels, const QStringList &outputLabels) {

	mNumOfInputs = numOfInputs;
	mNumOfOutputs = numOfOutputs;

	mNumOfRows = 1U << numOfInputs;

	mInputLabels = inputLabels;
	mOutputLabels = outputLabels;

	assertTruthTableConsistency();
}

TruthTable::TruthTable(QFile &file) {
    readFromFile(file);
}

TruthTable::TruthTable(const kvdm::LineVector &vec)
{
    Q_ASSERT(vec.length() > 0);

    mNumOfInputs = vec.at(0).getNumOfInputs();
    mNumOfOutputs = vec.at(0).getNumOfOutputs();

    mTable = vec;
}




QString TruthTable::getTruthTableAsQString() const {
	QString out;

	for (auto const & elem : mTable){
		out += elem.getLineAsQString() + "\n";
	}

	return out;
}

void TruthTable::addLine(kvdm::Line const &line) {
	mTable.append(line);
}


void TruthTable::readFromFile(QFile & file) {
	qDebug() << "Reading file " << file.fileName();

	mTable.clear();

	FSMState state = eStart;

	if (!file.isOpen()) {
		if (!file.open(QFile::ReadOnly | QFile::Text)) {
			qFatal("Error while attempting to open file.");
		}
	}

	QTextStream in(&file);

	if (in.status() != QTextStream::Status::Ok || in.atEnd() ){
		qFatal("Error while opening text Stream!");
	}

	QString line = in.readLine();

	// Reads the file line by line and handles it via a FSM
	// FSM may not be really necessary, due to the simple file format
	// may make it easier to expand to more variables
	while (state != eEnd){
		processLine(line, state);

		if (state == eError){
			qFatal("Error in state machine!");
		}

		line = in.readLine();
	}

	qDebug() << "Read from file:";
	qDebug() << "Number of Inputs: "	<< getNumOfInputs();
	qDebug() << "Number of Outputs: "	<< getNumOfOutputs();
	qDebug() << "Number of Rows: "		<< getNumOfRows();

}

void TruthTable::assertTruthTableConsistency() {
	//Checking, that the truth table is consistent
	//lengths must match, etc.

	qDebug() << "===Checking consistency of new Table===";

	//Check consitency of every line
	for (auto const & elem : mTable){
		Line::assertLineConsistency(elem);
	}

	qDebug() << "# of Inputs: " << getNumOfInputs();
	qDebug() << "# of Outputs: "<< getNumOfOutputs();
	qDebug() << "# of Rows: "	<< getNumOfRows();

	Q_ASSERT(getNumOfInputs()	<= kvdm::maxNumOfInputs);
	Q_ASSERT(getNumOfRows()		<= kvdm::maxNumOfRows);
	Q_ASSERT(getNumOfOutputs()	<= kvdm::maxNumOfOutputs);

	qDebug() << "# of Inputlabels: " << mInputLabels.length();
	qDebug() << "# of Outputlabels: "<< mOutputLabels.length();

	Q_ASSERT(mInputLabels.length() == getNumOfInputs());
	Q_ASSERT(mOutputLabels.length() == getNumOfOutputs());
}

//State machine for reading the file
void TruthTable::processLine(QString const &line, TruthTable::FSMState & state) {
	if (line.startsWith('#') || line.length() == 0){
		return;
	}

	if (line.isNull()) {
		qFatal("File ended before .e was found!");
	}


	QStringList lineSplit = line.split(" ");

	switch (state) {

		// Initial state
		// Looking for ".i"
		case eStart:
			if (line.startsWith(".i")) {
				state = eInFound;
				mNumOfInputs = lineSplit.at(1).toUInt();
			}
			else {
				state = eError;
			}
			break;

			//Error State
			// Reached, if the file format is wrong or the file cannot be read
		case eError:
			qWarning() << "Error in State machine! File my not be read correctly" << endl
					   << "Error occured in this line: " << line;
			state = eEnd;
			break;

			// ".i" has been found, looking for ".o"
		case eInFound:
			if (line.startsWith(".o")) {
				state = eInOutFound;
				mNumOfOutputs = lineSplit.at(1).toUInt();
			}
			else {
				state = eError;
			}
			break;

			// ".i" and ".o" have been found, looking for Input labels
		case eInOutFound:
			if (line.startsWith(".ilb")){
				state = eInLblFound;
				//get rid of first element, since it is equal to ".ilb"
				lineSplit.pop_front();

				mInputLabels = lineSplit;
			}
			else {
				state = eError;
			}
			break;

			//Looking for the output labels
		case eInLblFound:
			if (line.startsWith(".olb")){
				state = eAllFound;
				//get rid of first element, since it is equal to ".ilb"
				lineSplit.pop_front();

				mOutputLabels = lineSplit;
			}
			else {
				state = eError;
			}
			break;

			// ".i", ".o", ".ilb" and ".olb" have been found, now looking for ".p", which
			// marks the start of the truth table
		case eAllFound:
			if (line.startsWith(".p")){
				state = eProcessTruthTable;
			}
			else {
				state = eError;
			}
			break;

			//Read in Truth table line by line and append line to the truth table
		case eProcessTruthTable:
			if (line.startsWith(".e")){
				state = eEnd;
			}
			else {
				//Split line up into 2 parts, and read them in in REVERSE order (otherwise the highest Index in the vector would be the LSB)
				//This means, if you want to print something, you have to do it from highest Indes to lowest
				QString front = lineSplit.at(0);
				QString back = lineSplit.at(1);
				std::reverse(front.begin(), front.end());

				addLine(Line(front, back.at(0)));

			}
			break;
		case eEnd:
			qDebug() << "Successfully read file! State eEnd reached.";
			break;
	}

}

kvdm::Line TruthTable::getLine(int idx) {
	Q_ASSERT(idx > -1);

	if (idx < getNumOfRows()){
		return mTable.at(idx);
	}

	//return empty Line
	return Line();
}

void TruthTable::setTable(const kvdm::LineVector &vec) {
	mTable = vec;
}

/* Unused function
kvdm::CubeList TruthTable::getTableAsCubeList() const {
	return mTable.toList();
}
*/

kvdm::LineVector TruthTable::getTableAsQVector() const {
	return mTable;
}

LineVector TruthTable::getExpandedTableAsQVector() {
	kvdm::LineVector retVal;

	for (auto const & line : mTable) {
		retVal.append(line.expandLine());
	}

	std::sort(
		retVal.begin(),
		retVal.end(),
		[](kvdm::Line const & lhs, kvdm::Line const & rhs){ return lhs.inputsToUInt() < rhs.inputsToUInt(); }
	);

	return retVal;
}
