// MIP
#ifndef KVDM_GLOBALS_H
#define KVDM_GLOBALS_H

//System Includes
#include <QVector>
#include <QByteArray>
#include <QDebug>
#include <algorithm>

//Header file, with many Global Constants and variables

/*
 *Remark: the Abreviation QMC refers to the Quine McCluskey Minimisation method
 *
 * Side Note: Tables are represented as follows:
 * A B C D | Y
 * --------|--
 * 0 0 0 0 | 1
 * 0 0 0 1 | 0
 * 0 0 1 0 | 1
 *	etc.
 *
 * Where A, B, C, D are the inputs
 * and A is the most significant bit
 *
 * Y represents the Output (in a separate column)
 * If a vector representing bits is returned, then the leftmost index
 * represents the most significant bit (as if you defined it as X downto 0 in VHDL)
 * eg:
 *
 * Index  3 2 1 0
 *		 ---------
 * Value |0|1|0|1|
 *		 ---------
 * This vector represents the binary number 5
 */

namespace kvdm {

class Line;

// The initial scope of this programm is limited to 4 Input variables and 1 Output
// These restrictions are maybe temporary, who knows
// Most of the code is written, so that in theory it should work with more variables
// without major problems
static const int maxNumOfInputs = 4;
static const int maxNumOfOutputs = 1;

// Number of rows is equal to 2^NumberOfInputs
static const int maxNumOfRows = 1U << maxNumOfInputs;

// Type used to represent the individual bits within the program
// using char makes reading and printing easy, since you can just use
// the ASCCII Values of these chars
using bitType = char;

using Cube = QByteArray;

// 3 Major types
// ON:			represents the binary 1, locigal 'on' or 'yes'
// OFF:			represents the binary 0, logical 'off' or 'no'
// DON'T CARE:	represents either 1 or 0 (sometimes also represented as 'X')
static const bitType ON = '1';
static const bitType OFF = '0';
static const bitType DC = '-';

// Vector of Bits
using LineVector = QVector<Line>;

// Used in QMC to store the sorted minterms
using GroupVector = QVector<kvdm::LineVector>;

// Converts an unsigned number to an array consisting of chars of value '1' or '0'
// if the array is too small, it just fills in, whatever fits
// The highest Index in the vector represents the MSB
inline void convertToBinary(kvdm::Cube& array, size_t num)
{
	QString res;

	res.setNum(num, 2);

	std::reverse(res.begin(), res.end());

	array = res.toLocal8Bit();
}

inline unsigned int convertToUnsigned(kvdm::Cube const& cube)
{
	QString num = cube;
	bool res;

	auto retVal = num.toUInt(&res, 2);

	Q_ASSERT(res);

	return retVal;
}

} // namespace kvdm

#endif // KVDM_GLOBALS_H
