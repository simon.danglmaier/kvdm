
//System Includes
#include <QDebug>
#include <algorithm>

//Private Includes
#include "line.h"

//resolve namespace
using namespace kvdm;

//-------------------
//Constructors
//-------------------
Line::Line(kvdm::Cube const &in, bitType const out) {
	mInputs = in;
	mOutput = out;

	mIsMarked = false;
}

Line::Line(QString in, bitType const out) {

	Q_ASSERT(	out == kvdm::DC ||
				out == kvdm::ON ||
				out == kvdm::OFF);

	Q_ASSERT(in.length() < kvdm::maxNumOfInputs);

	for (auto const & elem : in) {
		Q_ASSERT(	elem == kvdm::DC ||
					elem == kvdm::ON ||
					elem == kvdm::OFF);
	}

	std::reverse(in.begin(), in.end());

	mInputs = in.toLocal8Bit();
	mOutput = out;

	mIsMarked = false;

	assertLineConsistency(*this);
}

Line::Line(Line const & line) {
	mInputs = line.mInputs;
	mOutput = line.mOutput;

	mIsMarked = line.mIsMarked;
}

Line::Line() {
	mIsMarked = false;
	mOutput = kvdm::OFF;
}

Line::Line(QString in, QChar const out){

	Q_ASSERT(	out == kvdm::DC ||
				out == kvdm::ON ||
				out == kvdm::OFF);

	Q_ASSERT(in.length() <= kvdm::maxNumOfInputs);

	for (auto const & elem : in) {
		Q_ASSERT(	elem == kvdm::DC ||
					elem == kvdm::ON ||
					elem == kvdm::OFF);
	}

	std::reverse(in.begin(), in.end());

	mInputs = in.toLocal8Bit();
	mOutput = out.toLatin1();

	mIsMarked = false;

	assertLineConsistency(*this);
}

//-------------------
//Getters
//-------------------

int Line::getNumberOfOnes() const
{
	return static_cast<int>(std::count(mInputs.cbegin(), mInputs.cend(), kvdm::ON));
}

size_t Line::calcHammingDistance(const Line& lhs, const Line& rhs)
{
	size_t diffs = 0;

	//Comparing 2 Lines of different lengths shouldnt happen anyway, this is just a sanity check
	Q_ASSERT(lhs.getInputs().length() == rhs.getInputs().length());

	for (auto i = 0; i < lhs.getInputs().length(); i++) {
		if(lhs.getInputs().at(i) != rhs.getInputs().at(i)) {
			diffs++;
		}
	}
	return diffs;
}

void Line::assertLineConsistency(const Line &line) {
	qDebug() << "===Checking consistency of new Line===";

	//Asserts, that the data of a Line is consitent with the limitations of this Bachelor thesis
	//For debugging and avoiding error cases
	qDebug() << "# of Inputs: " << line.getNumOfInputs();
	qDebug() << "# of Outputs: "<< line.getNumOfOutputs();
	Q_ASSERT(line.getInputs().length() <= kvdm::maxNumOfInputs);

	qDebug() << "Inputs: " << line.getInputs();
	qDebug() << "Ouputs: " << line.getNumOfOutputs();

	//Assert, that only 1's, 0's and dont-care's are in the inputs
	for (auto const & elem : line.getInputs()){
		Q_ASSERT(elem == kvdm::DC ||
				 elem == kvdm::ON ||
				 elem == kvdm::OFF);
	}

	//This is getting into super paranoid teritory
	Q_ASSERT(line.getInputs().length() == line.getNumOfInputs());

	qDebug() << "===SUCCESS: line is consitent===";
}


void Line::markLine(bool val) {
	mIsMarked = val;
}

bool Line::fuseLines(const Line &line) {
	if (calcHammingDistance(*this, line) != 1) {
		return false;
	}

	//Set it to invalid value at start, to verify results later
	int index = 0;

	while(mInputs.at(index) == line.mInputs.at(index)) {
		index++;
	}

	//Sanity check number 2:
	//This case shouldnt happen and be caught while reading in the file
	Q_ASSERT(line.mInputs.at(index) != kvdm::DC);

	//At the index, at which the 2 cubes differ, there is now supposed to be a dont-care
	mInputs[index] = kvdm::DC;

	return true;
}

QString Line::getLineAsQString() const {
	QString inputs(mInputs);

	//std::reverse(inputs.begin(), inputs.end());

	return inputs + " | " + mOutput;
}

bool Line::operator==(const Line &rhs) const {
	return (rhs.getInputs() == getInputs()) &&
		   (rhs.getOutput() == getOutput());
}

bool Line::operator<(const Line &rhs) const {
	//A Line is smaller than another one, if its number of 1's is smaller in the input column
	//Output is not considered, due to the fact, that the Quine-McCuskey Method is only concerned with
	//Cubes with Ouput 1 anyway

	//Sanity check
	Q_ASSERT(getInputs().length() == rhs.getInputs().length());

	return mInputs.count(kvdm::ON) < rhs.getInputs().count(kvdm::ON);
}

kvdm::LineVector Line::expandLine() const {
	kvdm::LineVector retVal;
	size_t changes = 1;

	retVal.append(*this);

	while (changes != 0) {
		changes = 0;
		for (auto i = 0; i < retVal.length(); i++) {
			for (auto j = retVal.at(i).getNumOfInputs() - 1; j >= 0; j--) {
				if (retVal.at(i).getInputs().at(j) == kvdm::DC) {

					//Set first DC to 0
					retVal[i].setInput(kvdm::ON, j);

					//Copy Line
					retVal.append(retVal[i]);

					//Set that lines DC to a 1
					retVal[i].setInput(kvdm::OFF, j);

					changes++;
				}
			}
		}
	}

	return retVal;
}

//-------------------
//Setters
//-------------------
void Line::setOutput(QChar const & ch){
	Q_ASSERT(	ch == kvdm::ON ||
				ch == kvdm::OFF ||
				ch == kvdm::DC);

	setOutput(ch.toLatin1());
}

void Line::setOutput(bitType const & ch){
	Q_ASSERT(	ch == kvdm::ON	||
				ch == kvdm::OFF	||
				ch == kvdm::DC);

	mOutput = ch;
}

void Line::setInput(bitType const& ch, int const& idx)
{
	Q_ASSERT(ch == kvdm::ON || ch == kvdm::OFF || ch == kvdm::DC);

	Q_ASSERT(idx > -1);

	mInputs[idx] = ch;
}

unsigned int Line::inputsToUInt() const {
	return convertToUnsigned(mInputs);
}
