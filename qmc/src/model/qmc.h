// MIP
#pragma once

// System Includes
#include <QList>
#include <QPair>

// Private Includes
#include "globals.h"
#include "truthtablemodel.h"

// Also refered to as QMC
class qmc {
public:
	// QMC works primarily with a Vector of Linevectors
	// The type GroupVector stores the currently to be minimized table
	// Each index stores only lines with that number of ones
	// Eg: 	Idx 0: Only minterms with zero ones
	//		Idx 3: Only minterms with 3 ones
	//		etc.
	// Careful: Dont Care values do not count as 1s, eventhough they can also represent
	// ones in the logical sense

	// Main function, that actually does the minimizing
	// Returns the prime implicants
	static kvdm::LineVector minimizeTruthTable(kvdm::TruthTable const& tt);
};
