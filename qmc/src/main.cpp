#include "view/kvdm_mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    kvdm_mainwindow w;
    w.show();

    return a.exec();
}
